import React from 'react';
import logo from '../logo.svg';
import { Avatar } from 'antd';
import {useModel} from '../store'
import {
	MenuUnfoldOutlined,
	MenuFoldOutlined,
	SettingOutlined,
} from '@ant-design/icons';
import {
} from 'antd'

export default function Header() {
    let [collapsed, setCollapsed] = useModel('menuCollapsed')
    let toggleCollapsed = () => {
        setCollapsed(!collapsed)
    };
	return (
		<header className="ly-header">
			<div className="l-part">
				<div className="collapsed-btn">
					<span onClick={toggleCollapsed}>{React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}</span>
				</div>
				<div className="logo-box">
					<img src={logo} className="app-logo" alt="logo" />
					<a
						className="app-link"
						href="https://reactjs.org"
						target="_blank"
						rel="noopener noreferrer"
					> Learn React </a>
				</div>
			</div>
			<div className="r-panel">
				<Avatar className="avatar" src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
				<SettingOutlined />
			</div>
		</header>
	);
}
