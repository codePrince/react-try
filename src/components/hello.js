import React from 'react'
import {useModel} from '../store'
import Mock from 'mockjs'

export default ({words})=>{
    let [name, setName] = useModel('girls[0].name')
    let [car, setCar] = useModel('car')
    let handleTest = ()=>{
        setName("Lucy")
    }
    let handleSetCar =()=>{
        setCar({
            brand: 'tesla',
            price: Mock.Random.natural(10, 200) + 'w'
        })
    }

	return (
		<div className="hello">
            <fieldset>
                <legend>hello 组件---{words}---</legend>
                <p>name: {name}</p>
                <p>car color: {car.color}</p>
                <p>car brand: {car.brand}</p>
                <p>car price: {car.price}</p> 
                <button onClick={handleTest}>set name</button>&nbsp;
                <button onClick={handleSetCar}>set car</button>
            </fieldset>
		</div>
	)
}
