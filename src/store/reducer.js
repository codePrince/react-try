export function user(state, action) {
  switch (action.type) {
    case 'login':
      return {count: state.count + 1};
    case 'logout':
      return {};
    default:
      throw new Error();
  }
}