import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import PageLayout from './layout/page-layout.js'
import Sidemenu from './components/side-menu.js'
import Header from './components/header.js'
import routes from './routes.js'


function App() {
  return (
    <Router>
      <Header />
      <div className="ly-body">
        <Sidemenu />
        <PageLayout>
          <Switch>
            {routes.map((route, i) => (
              <Route path={route.path} key={i}>
                <route.component />
              </Route>
            ))}
          </Switch>
        </PageLayout>
      </div>
    </Router>
  )
}

export default App
