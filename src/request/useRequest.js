import { useState, useEffect } from 'react'
export default (apiPromise)=>{
    let [loading, setLoading] = useState(true)
    let [data, setData] = useState()
    useEffect(()=>{
        let status = true
        if(loading){
            apiPromise.then(({data})=>{
                if(status){
                    setLoading(false)
                    setData(data)
                }
            })
        }
        return ()=>{
            status = false
        }
    })
    return [loading, data]
}