import _axios from './config.js';
import Mock from 'mockjs'

Mock.setup({
    timeout: '100-600'
})
Mock.mock('/test', 'post',  '@email')

export default {
    test ({name} = {}) {
        return _axios.post('/test', {name});
    },
}