import React from 'react'

export default (props)=>{
    return (
        <div className="ly-content">
            <div className="page-position"></div>
            <div className="page-main">
                {props.children}
            </div>
        </div>
    )
}