import Dashbord from './views/dashbord.js';
import RequestDemo from './views/request-demo.js'
import ListDemo from '@/views/list-demo.js'

export default [
  {
    path: "/request-demo",
    component: RequestDemo,
  },
  {
    path: "/list-demo",
    component: ListDemo,
  },
  {
    path: "/",
    component: Dashbord
  },
];