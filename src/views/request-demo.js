import React from 'react'
import API from '../request/api.js'
import useRequest from '../request/useRequest.js'

export default ()=>{
    let [loading, data] = useRequest(API.test({name: 'ali'}))
    if(loading) {
        return (
            <div>loading...</div>
        )
    }

    return (
        <div>
            <h1>I'm request demo page</h1>
            <p>{data}</p>
        </div>

    ) 
}