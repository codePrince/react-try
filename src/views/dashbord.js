import React from 'react';
import Hello from '../components/hello.js'
import {useModel} from '../store'
import Mock from 'mockjs'

export default ()=>{
    let [girlName, setGirlname] = useModel('girls[0].name')
    let [car, setCar] = useModel('car')
    let handleClick = ()=>{
        setGirlname(Mock.Random.first())
    }
    let handleSetCar =()=>{
        setCar({
            color: 'blue'
        })
    }

    return(
        <div>
            <h1>I'm dashbord page</h1>
            <h2>======全局状态尝试======</h2>
            <fieldset>
                <legend>dashbord 组件</legend>
                <p>name: {girlName}</p>
                <p>car color: {car.color}</p>
                <p>car brand: {car.brand}</p> 
                <p>car price: {car.price}</p> 
                <button onClick={handleClick}>set name</button>&nbsp;
                <button onClick={handleSetCar}>set car</button>
            </fieldset>
            <Hello words={"hello you !"} />
        </div>
    )
}