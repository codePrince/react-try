import React from 'react'
import API from '../request/api.js'
import {useModel} from '@/store'
import * as utils from '@/utils'
import { Button } from 'antd'
import _ from 'lodash'

export default (props)=>{
    let [cats, setCats] = useModel('cats')
    function add_one(){
        let _cats = _.clone(cats)
        _cats.push({
            name: "cat_" + new Date().getTime()
        })
        // console.log(cats)
        setCats(_cats)
    }
    return (
        <div>
            <Button onClick={add_one}>add one</Button>
            {utils.render_list(cats,(item, index)=>{
                return(
                    <div className="l-row" key={index}>{item.name}</div>
                )
            })}
        </div>
    ) 
}