export function render_list(list,fn){
    var vnode=[];
    try{
        list.forEach((item,index)=>{
            vnode.push(fn(item,index));
        });
    }catch(e){
		console.log(e)
    }
    return vnode;
}
export function render_obj(obj,fn){
	var piece=[];
	for(let prop in obj){
		piece.push(fn(prop,obj[prop]))
	}
	return piece;
}